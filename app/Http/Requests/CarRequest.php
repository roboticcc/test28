<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CarRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'manufacture_year' => 'integer|min:1950|max:2023',
            'mileage' => 'integer',
            'color' => 'string',
            'brand' => 'required|string',
            'model' => 'required|string',
        ];
    }
}
