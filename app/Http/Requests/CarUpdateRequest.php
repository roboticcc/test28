<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CarUpdateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'manufacture_year' => 'integer|min:1950|max:2023',
            'mileage' => 'integer',
            'color' => 'string',
            'brand' => 'string',
            'model' => 'string',
        ];
    }
}
