<?php

namespace App\Http\Controllers;

use App\Models\CarModel;
use App\Transformers\CarModelTransformer;

class CarModelController extends Controller
{
    public function index(): string
    {
        $models = CarModel::all();

        return fractal()
            ->collection($models)
            ->transformWith(new CarModelTransformer())
            ->toJson(JSON_PRETTY_PRINT);
    }
}
