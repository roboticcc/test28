<?php

namespace App\Http\Controllers;

use App\Models\CarBrand;
use App\Transformers\CarBrandTransformer;

class CarBrandController extends Controller
{
    public function index(): string
    {
        $brands = CarBrand::all();

        return fractal()
                ->collection($brands)
                ->transformWith(new CarBrandTransformer())
                ->toJson(JSON_PRETTY_PRINT);
    }
}
