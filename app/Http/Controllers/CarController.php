<?php

namespace App\Http\Controllers;

use App\Http\Requests\CarRequest;
use App\Http\Requests\CarUpdateRequest;
use App\Models\Car;
use App\Services\CarService;
use App\Transformers\CarTransformer;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class CarController extends Controller
{
    public function __construct(
        private readonly CarService $service
    )
    {
    }

    public function index(): string
    {
        $cars = $this->service->getCarsForUser();

        return fractal()
            ->collection($cars)
            ->transformWith(new CarTransformer())
            ->toJson(JSON_PRETTY_PRINT);
    }

    public function store(CarRequest $request): JsonResponse
    {
        $request = $request->validated();

        try {
            $this->service->createCar($request);

            return response()->json(['message' => 'Car has been successfully created']);
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 500);
        }
    }

    public function show(Car $car): string
    {
        $this->authorize('view', $car);

        return fractal()
            ->item($car)
            ->transformWith(new CarTransformer())
            ->toJson(JSON_PRETTY_PRINT);
    }

    public function update(CarUpdateRequest $request, Car $car): JsonResponse
    {
        $this->authorize('update', $car);
        $request = $request->validated();

        try {
            $this->service->updateCar($request);

            return response()->json(['message' => 'Car has been successfully updated']);
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 500);
        }
    }

    public function destroy(Car $car): JsonResponse
    {
        $this->authorize('delete', $car);
        $this->service->deleteCar($car);

        return response()->json(['message' => 'Car has been deleted successfully']);
    }
}
