<?php

namespace App\Policies;

use App\Models\Car;
use App\Models\User;
use Illuminate\Auth\Access\Response;

class CarPolicy
{
    public function view(User $user, Car $car): Response
    {
        return $user->id == $car->owner_id ? Response::allow() : Response::deny('You are not the owner of this car.', 403);
    }

    public function create(User $user): bool
    {
        return true;
    }

    public function update(User $user, Car $car): Response
    {
        return $user->id == $car->owner_id ? Response::allow() : Response::deny('You are not the owner of this car.', 403);
    }

    public function delete(User $user, Car $car): Response
    {
        return $user->id == $car->owner_id ? Response::allow() : Response::deny('You are not the owner of this car.', 403);
    }
}
