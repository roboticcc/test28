<?php

namespace App\Transformers;

use App\Models\CarBrand;
use League\Fractal\TransformerAbstract;

class CarBrandTransformer extends TransformerAbstract
{
    public function transform(CarBrand $brand): array
    {
        return [
            'brand_name' => $brand->name,
        ];
    }
}
