<?php

namespace App\Transformers;

use App\Models\CarModel;
use League\Fractal\TransformerAbstract;

class CarModelTransformer extends TransformerAbstract
{
    public function transform(CarModel $model): array
    {
        return [
            'model_name' => $model->name,
            'brand_name' => $model->brand->name,
        ];
    }
}
