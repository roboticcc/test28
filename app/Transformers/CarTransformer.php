<?php

namespace App\Transformers;

use App\Models\Car;
use League\Fractal\TransformerAbstract;

class CarTransformer extends TransformerAbstract
{
    public function transform(Car $car): array
    {
        return [
            'id' => $car->id,
            'manufactured_at' => $car->manufacture_year ?? null,
            'mileage' => $car->mileage ?? null,
            'color' => $car->color ?? null,
            'brand_name' => $car->brand->name,
            'model_name' => $car->model->name,
        ];
    }
}
