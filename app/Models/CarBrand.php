<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class CarBrand extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
    ];

    public function models(): HasMany
    {
        return $this->hasMany(CarModel::class, 'brand_id', 'id');
    }

    public function cars(): BelongsTo
    {
        return $this->belongsTo(Car::class, 'id', 'brand_id');
    }

    public static function createByName($name)
    {
        return self::create([
            'name' => $name
        ]);
    }
}
