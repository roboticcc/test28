<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\Auth;

class Car extends Model
{
    use HasFactory;

    protected $guarded = [
        'owner_id',
    ];

    public function brand(): HasOne
    {
        return $this->hasOne(CarBrand::class, 'id', 'brand_id');
    }

    public function model(): HasOne
    {
        return $this->hasOne(CarModel::class, 'id', 'model_id');
    }

    public static function boot(): void
    {
        parent::boot();

        static::creating(function ($car) {
            $car->owner_id = Auth::id();
        });
    }
}
