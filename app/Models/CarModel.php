<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class CarModel extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'brand_id',
    ];

    //Одна модель в теории может называться одинаково у нескольких брендов, но я счёл это не особо реалистичным
    public function brand(): BelongsTo
    {
        return $this->belongsTo(CarBrand::class, 'brand_id', 'id');
    }

    public function cars(): BelongsTo
    {
        return $this->belongsTo(Car::class, 'id', 'brand_id');
    }
}
