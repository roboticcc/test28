<?php

namespace App\Services;

use App\Models\Car;
use App\Models\CarBrand;
use App\Models\CarModel;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CarService
{
    public function createCar(array $data): mixed
    {
        DB::beginTransaction();

        try {
            $brand = CarBrand::firstOrCreate([
                'name' => $data['brand']
            ]);


            $model = CarModel::firstOrCreate([
                'name' => $data['model'],
                'brand_id' => $brand->id
            ]);

            //простой create используется намеренно, так как у одного пользователя может быть несколько идентичных машин
            $car = Car::create([
                'manufacture_year' => $data['manufacture_year'] ?? null,
                'mileage' => $data['mileage'] ?? null,
                'color' => $data['color'] ?? null,
                'brand_id' => $brand->id,
                'model_id' => $model->id,
            ]);

            DB::commit();

            return $car;
        } catch (\Throwable $e) {
            DB::rollBack();

            return $e->getMessage();
        }
    }

    public function updateCar(array $data): mixed
    {
        DB::beginTransaction();

        try {
            $brand = CarBrand::firstOrCreate([
                'name' => $data['brand']
            ]);

            $model = CarModel::firstOrCreate([
                'name' => $data['model'],
                'brand_id' => $brand->id
            ]);

            $car = Car::updateOrCreate([
                'manufacture_year' => $data['manufacture_year'] ?? null,
                'mileage' => $data['mileage'] ?? null,
                'color' => $data['color'] ?? null,
                'brand_id' => $brand->id,
                'model_id' => $model->id,
            ]);

            DB::commit();

            return $car;
        } catch (\Exception $e) {
            DB::rollBack();

            return $e->getMessage();
        }
    }

    public function getCarsForUser(): Collection
    {
        return Car::where('owner_id', '=', Auth::id())->get();
    }

    public function deleteCar(Car $car): void
    {
        $car->delete();
    }
}
