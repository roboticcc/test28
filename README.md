## Развёртывание проекта
1. В корне проекта выполняем `docker-compose up -d`
2. `docker-compose exec php-fpm composer install`
3. `docker-compose exec php-fpm php artisan env:decrypt --key=base64:p5De2FEqvMxphx3fsuF3xlgN0ylaYEJ6BuTW8H4UgfQ=`
4. `docker-compose exec php-fpm php artisan migrate`
5. `docker-compose exec php-fpm php artisan passport:install`
6. Всё готово, апишка доступна на localhost:8080

## API-методы

* <span style="background: green; color: white">POST</span> `/api/register` -- принимает объект с полями `name`, `email`, `password` и `password_confirmation`.
  Возвращает поле `token`, содержащее Bearer-токен в случае успешной регистрации, в ином случае отдаёт `errors` со списком полей, не прошедших валидацию
* <span style="background: green; color: white">POST</span>`/api/login` -- принимает объект с полями `email` и `password`, в случае успешного логина возвращает `token`, который содержит Bearer-токен для дальнейших запросов, в противном случае отдаёт объект с ошибками логина
* <span style="background: green; color: white">POST</span>`/api/logout` -- принимает запрос с пустым телом и отзывает токен у пользователя, выполнившего запрос


* <span style="background: blue; color: white">GET</span>`/api/cars` -- возвращает список машин, владельцем которых является текущий пользователь 
* <span style="background: blue; color: white">GET</span>`/api/cars/:id` -- возвращает машину с указанным ID, доступно только для пользователя, являющегося владельцем машины
* <span style="background: green; color: white">POST</span>`/api/cars` -- создаёт машину с указанными параметрами
* <span style="background: orange; color: white">PATCH</span>`/api/cars/:id` -- редактирует машину с указанным ID, доступно только для пользователя, являющегося владельцем машины
* <span style="background: red; color: white">DELETE</span>`/api/cars/:id` -- удаляет машину с указанным ID, доступно только для пользователя, являющегося владельцем машины


* <span style="background: blue; color: white">GET</span>`/api/models` -- возвращает все доступные модели машин и их марки

* <span style="background: blue; color: white">GET</span>`/api/brands` -- возвращает все доступные марки машин


## Прочее

* Переменные окружения были убраны из docker-compose для простоты развёртки (третий пункт)
* Апишка принимает данные в формате JSON и x-www-urlencoded, отдаёт в формате JSON, для корректного отображения ответов нужно установить хэдер "Accept: application/json" 
* Фронта нет, оставил в качестве главной страницы стандартную заглушку Laravel
* Токен авторизации активен 30 дней для простоты тестирования
