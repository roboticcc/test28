<?php

use App\Http\Controllers\Auth\ApiAuthController;
use App\Http\Controllers\CarBrandController;
use App\Http\Controllers\CarController;
use App\Http\Controllers\CarModelController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('/login', [ApiAuthController::class, 'login'])->name('login.api');
Route::post('/register',[ApiAuthController::class, 'register'])->name('register.api');

Route::middleware('auth:api')->group( function () {
    Route::apiResource('brands', CarBrandController::class)->only(['index']);
    Route::apiResource('models', CarModelController::class)->only(['index']);
    Route::apiResource('cars', CarController::class);

    Route::post('/logout', [ApiAuthController::class, 'logout'])->name('logout.api');
});
